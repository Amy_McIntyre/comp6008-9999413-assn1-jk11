﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmyMcIntyre_RandomNumber;

namespace AmyMcIntyre_RandomNumber
{
    class Users
    {
        public int answer2;
        public static int wins;
        public Users(int _digits)
        {
            answer2 = _digits;
        }

        public string GreenOrRed(int digits)
        {
            int numbers;
            string rightorwrong;

            numbers = digits;

            if (numbers == answer2)
            {
                rightorwrong = "#79ff4d";
            }
            else
            {
                rightorwrong = "#ff6e51";
            }
            return rightorwrong;
        }
        public int NewNumbers()
        {
            Random randomnum = new Random();
            int random = randomnum.Next(1, 10);
            return random;
        }

        public int NumWins (int RandomNumber)
        {
            int num;
            num = answer2;
            if (num == RandomNumber)
            {
                wins++;
            }
            return wins;
            
        }


    }
}
