﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmyMcIntyre_RandomNumber
{
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            InitializeComponent();
        }

        public void Button1(object sender, EventArgs e)
        {
            //var userinput = new Entry { Placeholder = "Guess" };

            var digits = int.Parse(userinput.Text);

            var Number = new Users(digits);

            var RandomNumber = Number.NewNumbers();
            var GreenOrRed = Number.GreenOrRed(RandomNumber);

            Background.BackgroundColor = Color.FromHex($"{GreenOrRed}");

            answer.Text = (RandomNumber.ToString());

            if (digits < 1 || digits > 10)
            {
                alert.Text = "Please Enter a number between 1 and 10";
            }

            else
            {
                alert.Text = "You have entered a number between 1 and 10";
            }


            var a = new Users(digits = int.Parse(userinput.Text));

            var WL = a.NumWins(RandomNumber);
            answer.Text = ($"Wins:{WL}  Random Number: {RandomNumber}");
        }

        private void button_Clicked(object sender, EventArgs e)
        {

        }
    }
}
