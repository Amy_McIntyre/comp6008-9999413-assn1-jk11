﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmyMcIntyre_Multipules
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            alert.Text = "This App is to show you the multipules of a number you enter. For example if you enter the number 10, the multipules of 10 are 3, 5, 6 and 9.These numbers added up add upto 23.So enter a number and find out what the multipules for that number are all added together.";
        }

        public void Button1(object sender, EventArgs e)
        {
            var userAnswer = inputnumber.inputs(userinput.Text);
            answer.Text = userAnswer.ToString();

            if(userAnswer < 1)
            {
                alert.Text = "Please enter a number higher than one";
            }

        }
    }
}
